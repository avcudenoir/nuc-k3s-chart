## Dependencies

_Pro tip: homebrew can also be used on Linux_

* helm (3.X): `brew intall helm`
* helmfile: `brew install helmfile`
* helm-diff plugin: `helm plugin install https://github.com/databus23/helm-diff`
* helm-secrets plugin: `helm plugin install https://github.com/futuresimple/helm-secrets`
