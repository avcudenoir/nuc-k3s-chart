## 1.12.0 (June 7, 2020)

CHANGED:

  * PVCs (Persistent Volume Claims) are now supported for Deployments (`.Values.applications.<app>.persistentVolumeClaims`)
    * example snippet:
        ```
          persistentVolumeClaims:
            storage:
              accessModes:
                - ReadWriteOnce
      
              storageClassName: "local-path"
              storageSize: "20Gi"
      
              automount:
                enable: true
                path: "/data"
        ```
    * in StatefulSets `volumeClaimTemplates` and container `volumeMounts` need to be manually injected in the chart values

BUGFIX:

* update the StatefulSet template in order to make it work nicely with newer Kubernetes versions (tested on 1.18)
    * `.spec.strategy` renamed to `.spec.updateStrategy`
    * added the `.spec.ServiceName` field
    
POSSIBLE BREAKING CHANGES:

*  the updated StatefulSet might stop working on older Kubernetes clusters

## 1.11.0 (February 11, 2020)

CHANGED:

  * the `replicaCount` is now optional for Deployments and ReplicaSets 

## 1.10.3 (November 8, 2019)

BUGFIX FOR: 

    *  error calling include: template: recruitment-service/charts/common-resource-templates/templates/_utils.yaml:19:32: executing "common.chart-fullname" at <tpl>: wrong number of args for tpl: want 2 got 1

## 1.10.0 (November 4, 2019)

BREAKING CHANGES:

  * Prometheus' `ServiceMonitor` resource and the corresponding `monitoring:` values block has been removed.
    
  * `$.Values.groupName`'s value can be templated

## 1.9.1 (October 2, 2019)

ADDED:

  * Deployments and StatefulSet resources now support custom-defined ServiceAccounts (under the `.Values.application.<APPLICATION_NAME>.serviceAccountName` field), will come in handy when trying to use [AWS-native method of injecting IAM roles to a k8s servciceaccount](https://aws.amazon.com/blogs/opensource/introducing-fine-grained-iam-roles-service-accounts/)

## 1.8.0 (September 23, 2019)

BREAKING CHANGES:

  * configmap/secret automounting in Deployments/Statefulsets

    No longer forced upon the user. Added the `(...).automount.enable` boolean field to choose whether automounting should be used.
    If false, the configmap/secret resources will be created, but `volumeMounts` and `volumes` fields wont we auto-populated.


  * changed the default set of resource labels

    Added the `app.kubernetes.io/` prefix.

    for example `app: <VALUE>` now becomes `app.kubernetes.io/app: <VALUE>`.


## 1.7.0 (August 26, 2019)

CHANGES:

  * HPA: instead of using a opinionated template for `spec.metrics` just use `toYaml` and let the chart user put whatever he wants to

## 1.6.1 (July 31,2019)

CHANGES:

  * ingress: additional paths will be injected first; the main path will be last, this will allow for blacklist-like behavior

## 1.6.0 (July 31, 2019)

BUGFIX:
 * deployment: `.Values.imagePullSecret` is now referenced by `$.Values.imagePullSecret`

CHANGES:
  * statefulset: added support for `imagePullSecret`
  * ingress: it's now possible to specify multiple paths for a single domain

BREAKING CHANGES:
  * `applications.YOURAPP.ingress.hosts.*.additionalPath` value is mandatory, can be empty (`[]`)

## 1.5.1 (June 18, 2019)

BUGFIX:
  * CPU metric utilization in Horizontal Pod Autoscaler's template now has a proper conditional

## 1.5.0 (June 17, 2019)

ADDED:

  monitoring.coreos.com/v1  // ServiceMonitor:
    * this CRD it now configurable using the `applications.YOURAPP.monitoring:` block

  general:
    * added `common.resources.all` template that includes ALL templated resources

## 1.4.0 (June 13, 2019)

CHANGES:
  Ingress:

  * add support for custom annotations, which come in handy when using the ALB ingress controller

## 1.3.1 (June 13, 2019)

CHANGES:
  Service:

  * When specifying `type: Loadbalancer`, the service template will no longer always expect to also have `service.staticIP` set

## 1.3.0 (June 11, 2019)
CHANGES:

  HorizontalPodAutoscaler:

  * changed the resource `apiVersion` from `autoscaling/v2beta1` to `autoscaling/v2beta2`
  * changed the default `spec.scaleTargetRef.apiVersion` from `apps/v1beta1` to `apps/v1`
  * `autoscaling.targetCPUUtilizationPercentage` is optional and thus the `cpu` metric is no longer added by default (you'll have to actually specify `autoscaling.targetCPUUtilizationPercentage` for it to be added)
  * `spec.maxReplicas` supports templating its value on the chart values-level (the `tpl` function)
