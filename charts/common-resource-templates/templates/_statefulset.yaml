{{- define "common.resources.statefulset" }}
{{- range $app, $parameters := .Values.applications }}
{{- if eq $parameters.controller "statefulset" }}
{{- $appPart := dict "appPart" $app }}
{{- $_ := merge $ $appPart }}
{{- $_ := set $ "appPart" $app }}
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ include "common.chart-fullname" $ }}
  labels:
    {{- include "common.metadata-labels" $ | nindent 4 }}
spec:
  updateStrategy:
    {{- toYaml $.Values.updateStrategy | nindent 4 }}
  {{- if $parameters.replicaCount }}
  replicas: {{ $parameters.replicaCount }}
  {{- end }}
  serviceName: {{ include "common.chart-fullname" $ }}
  selector:
    matchLabels:
      {{- include "common.dynamic-spec-labels" $ | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "common.dynamic-spec-labels" $ | nindent 8 }}
      {{- if $parameters.annotations }}
      annotations:
        {{- toYaml $parameters.annotations | nindent 8 }}
      {{- end }}
    spec:
      containers:
      {{- range $container, $containerParams := $parameters.containers }}
        - name: {{ $container }}
          image: {{ $containerParams.image | quote }}
          imagePullPolicy: {{ $.Values.imagePullPolicy }}
          command:
          {{- range $cmdPart := $containerParams.command }}
            - {{ tpl $cmdPart $ }}
          {{- end }}
          args:
          {{- range $argPart := $containerParams.args }}
            - {{ tpl $argPart $ | quote }}
          {{- end }}
          env:
          {{- range $name, $value := $containerParams.envs }}
            - name: {{ $name }}
              value: {{ tpl $value $ | quote }}
          {{- end }}
          {{- range $name, $parameters := $containerParams.secretEnvs }}
            - name: {{ $name }}
              valueFrom:
                secretKeyRef:
                  {{- if $parameters.secretRef }}
                  name: {{ tpl $parameters.secretRef $ }}
                  {{- end }}
                  {{- if $parameters.secretRefTemplate }}
                  name: {{ include $parameters.secretRefTemplate $ }}
                  {{- end }}
                  key: {{ tpl $parameters.key $ }}
          {{- end }}
          {{- if $containerParams.ports }}
          ports:
            {{- toYaml $containerParams.ports | nindent 12 }}
          {{- end }}
          {{- if $containerParams.readinessProbe }}
          readinessProbe:
            {{- toYaml $containerParams.readinessProbe | nindent 12 }}
          {{- end }}
          {{- if $containerParams.livenessProbe }}
          livenessProbe:
            {{- toYaml $containerParams.livenessProbe | nindent 12 }}
          {{- end }}
          volumeMounts:
          {{- if $containerParams.volumeMounts }}
            {{- toYaml $containerParams.volumeMounts | nindent 12 }}
          {{ end }}
          {{- if $parameters.configmaps }}
          {{- range $subresource, $rparameters := $parameters.configmaps }}
          {{- $_ := set $ "subresource" $subresource }}
          {{ if $rparameters.automount.enable }}
            - mountPath: {{ $rparameters.automount.path }}
              name: {{ include "common.subresource-fullname" $ }}
          {{- end }}
          {{- end }}
          {{- end }}
          {{- if $parameters.secrets }}
          {{- range $subresource, $rparameters := $parameters.secrets }}
          {{- $_ := set $ "subresource" $subresource }}
          {{ if $rparameters.automount.enable }}
            - mountPath: {{ $rparameters.automount.path }}
              name: {{ include "common.subresource-fullname" $ }}
          {{- end }}
          {{- end }}
          {{- end }}
          resources:
            {{- toYaml ( required "a non-empty resource: block is required!" $containerParams.resources ) | nindent 12 }}
      {{- end }}
      terminationGracePeriodSeconds: 30
      {{- if $parameters.serviceAccountName }}
      serviceAccountName: "{{ $parameters.serviceAccountName }}"
      {{- end }}
      volumes:
      {{- if $parameters.volumes }}
        {{- toYaml $parameters.volumes | nindent 8 }}
      {{- end }}
      {{- if $parameters.configmaps }}
      {{- range $subresource, $rparameters := $parameters.configmaps }}
      {{- $_ := set $ "subresource" $subresource }}
      {{ if $rparameters.automount.enable }}
        - name: {{ include "common.subresource-fullname" $ }}
          configMap:
              defaultMode: {{ $rparameters.automount.defaultMode }}
              name: {{ include "common.subresource-fullname" $ }}
      {{- end }}
      {{- end }}
      {{- end }}
      {{- if $parameters.secrets }}
      {{- range $subresource, $rparameters := $parameters.secrets }}
      {{- $_ := set $ "subresource" $subresource }}
      {{ if $rparameters.automount.enable }}
        - name: {{ include "common.subresource-fullname" $ }}
          secret:
              secretName: {{ include "common.subresource-fullname" $ }}
      {{- end }}
      {{- end }}
      {{- end }}
      {{- if eq $parameters.podAntiAffinity "hard" }}
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 1
              podAffinityTerm:
                topologyKey: failure-domain.beta.kubernetes.io/zone
                labelSelector:
                  matchLabels:
                    {{- include "common.dynamic-spec-labels" $ | nindent 20 }}
          requiredDuringSchedulingIgnoredDuringExecution:
            - topologyKey: "kubernetes.io/hostname"
              labelSelector:
                matchLabels:
                  {{- include "common.dynamic-spec-labels" $ | nindent 18 }}
      {{- else if eq $parameters.podAntiAffinity "soft" }}
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 1
              podAffinityTerm:
                {{/*
                  Needs verification if it applies to AWS nodes
                */}}
                topologyKey: failure-domain.beta.kubernetes.io/zone
                labelSelector:
                  matchLabels:
                    {{- include "common.dynamic-spec-labels" $ | nindent 20 }}
      {{- else if eq $parameters.podAntiAffinity "none" }}
      {{- end }}
      {{- if $parameters.nodeSelector }}
      nodeSelector:
        {{- toYaml $parameters.nodeSelector | nindent 8 }}
      {{- end }}
      {{- if $parameters.tolerations }}
      tolerations:
        {{- toYaml $parameters.tolerations | nindent 8 }}
      {{- end }}
      {{- if $.Values.imagePullSecret }}
      imagePullSecrets:
        - name: {{ $.Values.imagePullSecret }}
      {{- end }}
  {{- if $parameters.volumeClaimTemplates }}
  volumeClaimTemplates:
    {{- toYaml $parameters.volumeClaimTemplates | nindent 4 }}
  {{- end }}

...
{{- end }}
{{- end }}
{{- end }}
